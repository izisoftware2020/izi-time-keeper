import 'package:flutter/cupertino.dart';

class Categories {
  final int id;
  final String content;
  final Color color;

  const Categories({this.id,this.content,this.color});
}